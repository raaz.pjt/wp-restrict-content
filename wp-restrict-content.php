<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.convergetree.com/
 * @since             1.0.0
 * @package           Wp_Restrict_Content
 *
 * @wordpress-plugin
 * Plugin Name:       WP Restrict Content
 * Plugin URI:        https://www.convergetree.com/
 * Description:       The easiest way to restrict page and redirect it to another page.
 * Version:           1.0.0
 * Author:            Convergetree Technologies
 * Author URI:        https://www.convergetree.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-restrict-content
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WP_RESTRICT_CONTENT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-restrict-content-activator.php
 */
function activate_wp_restrict_content() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-restrict-content-activator.php';
	Wp_Restrict_Content_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-restrict-content-deactivator.php
 */
function deactivate_wp_restrict_content() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-restrict-content-deactivator.php';
	Wp_Restrict_Content_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_restrict_content' );
register_deactivation_hook( __FILE__, 'deactivate_wp_restrict_content' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-restrict-content.php';
/**
 * To include custom filed function
 */
require plugin_dir_path(__FILE__) . 'includes/CMB2/custom-fields.php';

/**
 * To include wp restrict function
 */
require plugin_dir_path(__FILE__) . 'includes/wp-restrict-function.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_restrict_content() {

	$plugin = new Wp_Restrict_Content();
	$plugin->run();

}
run_wp_restrict_content();

