(function ($) {
    'use strict';

    function display(value) {
        if (value == 'disable') {
            $(".cmb2-id-cv-domain-url").hide();
            $(".cmb2-id-cv-from-url").hide();
            $(".cmb2-id-cv-redirect-url").hide();
        } else if (value == 'enable') {
            $(".cmb2-id-cv-domain-url").show();
            $(".cmb2-id-cv-from-url").show();
            $(".cmb2-id-cv-redirect-url").show();
        }
    }
    $(document).ready(function () {
        var initial_val = $("input[name=cv_restrict_page][checked]").val();
        display(initial_val);
        $("input[name=cv_restrict_page]:radio").change(function () {
            var changed_value = $(this).val();
            display(changed_value);
        });
    });
})(jQuery);
