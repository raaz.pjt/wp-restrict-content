<?php
function cv_referer_function()
{

    if (!is_user_logged_in()) {
        $restrict_page = get_post_meta(get_the_ID(), 'cv_restrict_page', true);
        if ($restrict_page == 'enable') {
            $matched = '';
            $ref = $_SERVER['HTTP_REFERER'];
            $refData = parse_url($ref);
            $referer_rel = $refData['host'] . $refData['path'];
            $referer_domain = $refData['host'];
            $domain_list = get_post_meta(get_the_ID(), 'cv_domain_url', true);
            if ($domain_list) {
                $domain_list = explode(",", $domain_list);
                for ($i = 0; $i < count($domain_list); $i++) {
                    $domain = str_replace(' ', '', $domain_list[$i]);
                    $domain = str_replace(array('http://', 'https://'), '', $domain);
                    if ($domain == $referer_domain) {
                        $matched = true;
                        break;
                    }
                }
            }
            $url_list = get_post_meta(get_the_ID(), 'cv_from_url', true);
            if ($url_list) {
                $url_list = explode(",", $url_list);
                for ($i = 0; $i < count($url_list); $i++) {
                    $url = str_replace(' ', '', $url_list[$i]);
                    $url = str_replace(array('http://', 'https://'), '', $url);

                    if ($url == $referer_rel) {
                        $matched = true;
                        break;
                    }
                }
            }
            if ($matched !== true) {
                $redirectURL = get_post_meta(get_the_ID(), 'cv_redirect_url', true);
                if ($redirectURL) {
                    wp_redirect($redirectURL);
                } else {
                    $file_path = get_template_directory().'/404.php';
                    $fileExists = is_file($file_path);
                    if($fileExists){
                        wp_redirect(home_url('/404.php/'));
                    }else{
                        wp_redirect(home_url());
                    }
                }
                die;
            }
        }
    }
}
add_action('template_redirect', 'cv_referer_function');