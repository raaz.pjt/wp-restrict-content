<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.convergetree.com/
 * @since      1.0.0
 *
 * @package    Wp_Restrict_Content
 * @subpackage Wp_Restrict_Content/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Restrict_Content
 * @subpackage Wp_Restrict_Content/includes
 * @author     Convergetree Technologies <info@convergetree.com>
 */
class Wp_Restrict_Content_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
