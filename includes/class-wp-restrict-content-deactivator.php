<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.convergetree.com/
 * @since      1.0.0
 *
 * @package    Wp_Restrict_Content
 * @subpackage Wp_Restrict_Content/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Restrict_Content
 * @subpackage Wp_Restrict_Content/includes
 * @author     Convergetree Technologies <info@convergetree.com>
 */
class Wp_Restrict_Content_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
