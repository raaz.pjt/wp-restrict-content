<?php

/**
 * Relative Product Metaboxes
 */
if (file_exists(dirname(__FILE__) . '/init.php')) {
    require_once dirname(__FILE__) . '/init.php';
} elseif (file_exists(dirname(__FILE__) . '/init.php')) {
    require_once dirname(__FILE__) . '/init.php';
}


add_action('cmb2_admin_init', 'cv_wp_content_restrict');
function cv_wp_content_restrict()
{
    $prefix = 'cv_';
    /**
     * Initiate the metabox
     */
    $args=array(
        'public'                => true,
        'exclude_from_search'   => false,
        '_builtin'              => false
    );

    $output = 'names'; // names or objects, note names is the default
    $operator = 'and'; // 'and' or 'or'
    $post_types = get_post_types($args,$output,$operator);

    $initial_array = array('page','post');
    $posttypes_array = array();

    foreach ($post_types  as $post_type ) {
        $posttypes_array[] = $post_type;
    }
    $posttypes_array = array_merge($initial_array,$posttypes_array);

    $cmb = new_cmb2_box(array(
        'id' => 'redirect_url',
        'title' => __('Restrict This Content'),
        'object_types' => $posttypes_array, // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true,
    ));
    $cmb->add_field(array(
        'name' => __('Restrict Content'),
        'id' => $prefix . 'restrict_page',
        'type' => 'radio_inline',
        'options' => array(
            'enable' => __('Enable', 'cmb2'),
            'disable' => __('Disable', 'cmb2'),
        ),
        'default' => 'disable',
    ));
    $cmb->add_field(array(
        'name' => __('From Domain'),
        'desc'    => 'Example (https://www.convergetree.com,https://www.chatmuse.com)',
        'id' => $prefix . 'domain_url',
        'type' => 'textarea_small',
    ));
    $cmb->add_field(array(
        'name' => __('From URL'),
        'desc'    => 'Example (https://www.convergetree.com,https://www.chatmuse.com)',
        'id' => $prefix . 'from_url',
        'type' => 'textarea_small',
    ));
    $cmb->add_field(array(
        'name' => __('Redirect URL'),
        'desc'    => 'Example (https://www.convergetree.com)',
        'id' => $prefix . 'redirect_url',
        'type' => 'text',
    ));
}
